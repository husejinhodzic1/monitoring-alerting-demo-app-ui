import logo from './logo.svg';
import './App.css';
import CarCrud from "./CarCrud";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <CarCrud></CarCrud>
            </header>
        </div>
    );
}

export default App;

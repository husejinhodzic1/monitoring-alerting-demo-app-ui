import React, {useState, useEffect} from 'react';
import {
    Button,
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControlLabel,
    Radio,
    RadioGroup,
} from '@mui/material';
import axios from 'axios';

const apiUrl = 'http://localhost:8080/autokuca/automobili'; // Replace with your API endpoint

const CarCrud = () => {
    const [cars, setCars] = useState([]);
    const [newCar, setNewCar] = useState({
        marka: '',
        model: '',
        godinaProizvodnje: 0,
        boja: '',
        brojVrata: 0,
        snagaMotora: 0,
        zapreminaMotora: 0,
        gorivo: '',
        cena: 0,
        naServisu: 0,
    });
    const [open, setOpen] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false); // Indicates whether it's an update or add operation
    const [selectedCarId, setSelectedCarId] = useState(null); // ID of the selected car for update

    const fetchCars = async () => {
        try {
            const response = await axios.get(apiUrl);
            setCars(response.data);
        } catch (error) {
            console.error('Error fetching cars:', error);
        }
    };

    const deleteCar = async (id) => {
        try {
            await axios.delete(`http://localhost:8080/autokuca/obrisi_automobil/${id}`);
            fetchCars();
        } catch (error) {
            console.error('Error deleting car:', error);
        }
    };

    const handleInputChange = (event) => {
        const {name, value} = event.target;
        setNewCar({...newCar, [name]: value});
    };

    const handleRadioChange = (event) => {
        setNewCar({...newCar, naServisu: parseInt(event.target.value, 10)});
    };

    const handleOpen = (car) => {
        setOpen(true);
        setIsUpdate(!!car);
        setSelectedCarId(car ? car.id : null);

        if (car) {
            // If it's an update, set the existing car details in the dialog
            setNewCar({...car});
        } else {
            // If it's an add, reset the form
            setNewCar({
                marka: '',
                model: '',
                godinaProizvodnje: 0,
                boja: '',
                brojVrata: 0,
                snagaMotora: 0,
                zapreminaMotora: 0,
                gorivo: '',
                cena: 0,
                naServisu: 0,
            });
        }
    };

    const handleClose = () => {
        setOpen(false);
        setIsUpdate(false);
        setSelectedCarId(null);
    };

    const handleSubmit = async () => {
        if (isUpdate) {
            // Update the car
            try {
                await axios.put(`http://localhost:8080/autokuca/azuriraj_automobil/${selectedCarId}`, newCar);
                fetchCars();
                handleClose();
            } catch (error) {
                console.error('Error updating car:', error);
            }
        } else {
            // Add a new car
            try {
                await axios.post('http://localhost:8080/autokuca/dodaj_automobil', newCar);
                fetchCars();
                handleClose();
            } catch (error) {
                console.error('Error adding car:', error);
            }
        }
    };

    useEffect(() => {
        fetchCars();
    }, []); // Fetch cars on component mount


    return (
        <div>
            <div className="header">
                <h1>Auto kuca</h1>
            </div>
            <div className="container">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleOpen()}
                    className="addButton"
                >
                    Novo auto
                </Button>

                <Dialog open={open} onClose={handleClose}>
                    <DialogTitle>{isUpdate ? 'Azuriraj auto' : 'Dodaj novo auto'}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Please enter the details of the car.</DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Marka"
                            type="text"
                            fullWidth
                            name="marka"
                            value={newCar.marka}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Model"
                            type="text"
                            fullWidth
                            name="model"
                            value={newCar.model}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Godina Proizvodnje"
                            type="number"
                            fullWidth
                            name="godinaProizvodnje"
                            value={newCar.godinaProizvodnje}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Boja"
                            type="text"
                            fullWidth
                            name="boja"
                            value={newCar.boja}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Broj Vrata"
                            type="number"
                            fullWidth
                            name="brojVrata"
                            value={newCar.brojVrata}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Snaga Motora"
                            type="number"
                            fullWidth
                            name="snagaMotora"
                            value={newCar.snagaMotora}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Zapremina Motora"
                            type="number"
                            fullWidth
                            name="zapreminaMotora"
                            value={newCar.zapreminaMotora}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Gorivo"
                            type="text"
                            fullWidth
                            name="gorivo"
                            value={newCar.gorivo}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            label="Cena"
                            type="number"
                            fullWidth
                            name="cena"
                            value={newCar.cena}
                            onChange={handleInputChange}
                        />
                        <RadioGroup
                            aria-label="Na Servisu"
                            name="naServisu"
                            value={newCar.naServisu.toString()}
                            onChange={handleRadioChange}
                        >
                            <FormControlLabel value="1" control={<Radio/>} label="Na servisu"/>
                            <FormControlLabel value="0" control={<Radio/>} label="Nije na servisu"/>
                        </RadioGroup>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleSubmit} color="primary">
                            {isUpdate ? 'Update' : 'Add'}
                        </Button>
                    </DialogActions>
                </Dialog>

                <TableContainer component={Paper} className="tableContainer">
                    <Table className="table">

                        <TableHead>
                            <TableRow>
                                <TableCell>Marka</TableCell>
                                <TableCell>Model</TableCell>
                                <TableCell>Godina Proizvodnje</TableCell>
                                <TableCell>Boja</TableCell>
                                <TableCell>Broj Vrata</TableCell>
                                <TableCell>Snaga Motora</TableCell>
                                <TableCell>Zapremina Motora</TableCell>
                                <TableCell>Gorivo</TableCell>
                                <TableCell>Cena</TableCell>
                                <TableCell>Na Servisu</TableCell>
                                <TableCell>Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cars.map((car) => (
                                <TableRow key={car.id}>
                                    <TableCell>{car.marka}</TableCell>
                                    <TableCell>{car.model}</TableCell>
                                    <TableCell>{car.godinaProizvodnje}</TableCell>
                                    <TableCell>{car.boja}</TableCell>
                                    <TableCell>{car.brojVrata}</TableCell>
                                    <TableCell>{car.snagaMotora}</TableCell>
                                    <TableCell>{car.zapreminaMotora}</TableCell>
                                    <TableCell>{car.gorivo}</TableCell>
                                    <TableCell>{car.cena}</TableCell>
                                    <TableCell>{car.naServisu == 1 ? 'DA' : 'NE'}</TableCell>
                                    <TableCell>
                                        <Button variant="outlined" color="primary" onClick={() => handleOpen(car)}>
                                            Azuriraj auto
                                        </Button>
                                        <Button variant="outlined" color="secondary" onClick={() => deleteCar(car.id)}>
                                            Obrisi
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    );
};

export default CarCrud;
